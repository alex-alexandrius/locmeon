package com.turtlecat.locmeon;

public class Radar {
	String radarPlace;
	double radarLat;
	double radarLon;
	int speed;
	
	public Radar(String place,String lat, String lon, String s){
		radarPlace=place;
		radarLat = Double.parseDouble(lat);
		radarLon = Double.parseDouble(lon);
		speed = Integer.parseInt(s);
	}

	@Override
	public String toString() {
		return radarPlace + " " + radarLat+ " " + radarLon + " " + speed;
	}
}
