package com.turtlecat.locmeon;

import android.content.Context;
import android.content.SharedPreferences;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

public class BackgroundService extends Service implements LocationListener {
	public static double pLong, pLat;
	public static int distanceLeft;
	public static int maxSpeed, speedNow;
	public static Location tempLoc;
	public String provider;
	public Location loc;
	public LocationManager lm;
	public static String radarPlace;
	public static BackgroundService instance;
	Notification note;
	NotificationManager mNotificationManager;
	int notifyID = 1243;
	PendingIntent pi;
	public boolean isOn = false;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {

		super.onCreate();
		Criteria criteria = new Criteria();
		instance = this;
		lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		provider = lm.getBestProvider(criteria, false);
		provider = LocationManager.GPS_PROVIDER;
		loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 1, this);
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		note = new Notification(R.drawable.ic_launcher, "ანტიჯარიმა ჩართულია", System.currentTimeMillis());

		note.flags |= Notification.FLAG_NO_CLEAR;

		Intent notificationIntent = new Intent(this, MainActivity.class);

		pi = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		note.setLatestEventInfo(this, "ანტიჯარიმა", "ველოდები GPS-ის სიგნალს...", pi);

		isOn = true;

		startForeground(notifyID, note);

	}

	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onLocationChanged(Location l) {
		if (l != null) {

			pLong = l.getLongitude();
			pLat = l.getLatitude();
			tempLoc = l;
			distanceLeft = 999999999;
			maxSpeed = 0;
			for (int i = 0; i < MainActivity.radars.size(); i++) {
				if (distanceLeft > Integer.parseInt(Engine.getDistance(pLat, MainActivity.radars.get(i).radarLon, pLong,
						MainActivity.radars.get(i).radarLat)) && i != MainActivity.passedRadarIndexInArray) {
					distanceLeft = Integer.parseInt(Engine.getDistance(pLat, MainActivity.radars.get(i).radarLon, pLong,
							MainActivity.radars.get(i).radarLat));
					note.setLatestEventInfo(BackgroundService.this, "ანტიჯარიმა", "შემდეგ რადარამდე " + distanceLeft + "მ.", pi);
					mNotificationManager.notify(notifyID, note);
					radarPlace = MainActivity.radars.get(i).radarPlace;
					maxSpeed = MainActivity.radars.get(i).speed;
					if (distanceLeft <= 20) {
						MainActivity.passedRadarIndexInArray = i;
					}
				}
			}

			speedNow = (int) Math.round((l.getSpeed() * 3600) / 1000);

			SharedPreferences settings = getApplicationContext().getSharedPreferences("PREFS", 0);
			MainActivity.distanceToStartBeeping = settings.getInt("distanceFromRadar", 100);
			if (speedNow > maxSpeed) {
				if (distanceLeft < MainActivity.distanceToStartBeeping) {
					MainActivity.mp.start();
				}
			} else if (settings.getBoolean("signalWithoutSpeed", false)) {
				if (distanceLeft < MainActivity.distanceToStartBeeping) {
					MainActivity.mp.start();
				}
			}
			if (isOn)
				MainActivity.locUpdate();
		}
	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	public void stopRequestingGPS() {
		lm.removeUpdates(this);
		stopForeground(true);
		isOn = false;
		stopSelf();
		instance = null;
		tempLoc = null;
	}

	public void startRequestingGPS() {
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 1, this);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
