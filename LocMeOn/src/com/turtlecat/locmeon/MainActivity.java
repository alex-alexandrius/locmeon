package com.turtlecat.locmeon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.google.android.gms.ads.*;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.support.v4.app.FragmentActivity;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends FragmentActivity {
	public static final String PREFS = "PREFS";
	public static int distanceToStartBeeping;
	public static ArrayList<Radar> radars = new ArrayList<Radar>();
	public static MediaPlayer mp;
	static GoogleMap googleMap;
	MarkerOptions markerOptions;
	static TextView textDistance;
	static TextView textLarge;
	ImageButton buttonStartStop;
	public ImageButton buttonSettingsPage;
	public ImageButton buttonInfo;
	public LocationManager lm;
	static MarkerOptions markOptions;
	int arrayIndex = -1;
	public static int passedRadarIndexInArray = -1;
	static boolean firstStartMap = true;
	private PowerManager.WakeLock wl;

	static Marker mark;
	public String provider;
	Location loc;
	boolean isOn = false;
	public static boolean isAppOnMainScreen = false;
	public static Context context;

	public static void locUpdate() {
		if (isAppOnMainScreen) {
			// Toast.makeText(context.getApplicationContext(),
			// "IS IT EVEN HERE?", Toast.LENGTH_SHORT).show();
			textDistance.setText("უახლოეს რადარამდე " + BackgroundService.distanceLeft + " მ. " + BackgroundService.radarPlace);
			textLarge.setText(BackgroundService.speedNow + "/" + BackgroundService.maxSpeed + " KMH");
			if (googleMap != null) {
				if (firstStartMap) {
					CameraPosition newCamPos = new CameraPosition(new LatLng(BackgroundService.pLat, BackgroundService.pLong), 16, // zoom
							60, // დახრილობის კუთხე
							BackgroundService.tempLoc.getBearing()); // მიმართულებაზე
																		// დამოკიდებული
																		// კამერა
					googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 500, null);

					firstStartMap = false;
				} else {
					CameraPosition newCamPos = new CameraPosition(new LatLng(BackgroundService.pLat, BackgroundService.pLong),
							googleMap.getCameraPosition().zoom, // zoom
							googleMap.getCameraPosition().tilt, // დახრილობის
																// კუთხე
							BackgroundService.tempLoc.getBearing()); // მიმართულებაზე
																		// დამოკიდებული
																		// კამერა
					googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 500, null);

					firstStartMap = false;
				}
				if (mark != null)
					mark.remove();

				/*
				 * googleMap .animateCamera(CameraUpdateFactory.newLatLngZoom(
				 * new LatLng(pLat, pLong), 15));
				 */

				markOptions = new MarkerOptions().position(new LatLng(BackgroundService.pLat, BackgroundService.pLong)).title("თქვენ")
						.icon(BitmapDescriptorFactory.fromResource(R.drawable.point));
				mark = googleMap.addMarker(markOptions);
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = this;
		isAppOnMainScreen = true;

		AdView adView = (AdView) this.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

		SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS, 0);
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNjfdhotDimScreen");

		// Toast.makeText(getApplicationContext(),
		// Integer.toString(settings.getInt("distanceFromRadar", 100)),
		// Toast.LENGTH_SHORT).show();
		if (settings.getBoolean("keepScreenAlive", false)) {
			// Toast.makeText(getApplicationContext(), "HI",
			// Toast.LENGTH_SHORT).show();
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		boolean firstRun = settings.getBoolean("firstRun", true);

		if (firstRun) {
			// showFirstStartMessage();
			SharedPreferences set = getApplicationContext().getSharedPreferences(PREFS, 0);
			SharedPreferences.Editor editor = set.edit();
			editor.putBoolean("firstRun", false);
			editor.putBoolean("keepScreenAlive", true);
			editor.commit();

		}

		AssetManager am = getAssets();
		googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		// LatLngBounds bounds = new LatLngBounds(new LatLng(41.685585,
		// 44.738302), new LatLng(41.810948, 44.849796));
		/*
		 * BitmapDescriptor mapTbilisi =
		 * BitmapDescriptorFactory.fromResource(R.raw.tbilisi);
		 * googleMap.addGroundOverlay(new GroundOverlayOptions()
		 * .image(mapTbilisi) .positionFromBounds(bounds) .transparency(0));
		 */
		if (googleMap != null) {
			googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(41.71, 44.793), 10)); // თბილისის
																										// კოორდინატები
		}
		if (googleMap == null) {
			noGooglePlayServices();
		}

		mp = MediaPlayer.create(MainActivity.this, R.raw.notification);

		try {
			InputStream is = am.open("radars");

			String radar = null;
			int i = 0;
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			if (is != null) {
				while ((radar = reader.readLine()) != null) {
					String tempPlace, tempLat, tempLon, tempMaxSpeed;
					tempPlace = radar;
					tempLat = reader.readLine();
					tempLon = reader.readLine();
					tempMaxSpeed = reader.readLine();
					radars.add(new Radar(tempPlace, tempLat, tempLon, tempMaxSpeed));
					if (googleMap != null) {
						googleMap.addMarker(new MarkerOptions().position(new LatLng(radars.get(i).radarLat, radars.get(i).radarLon))
								.title(radars.get(i).radarPlace)
								.snippet("მაქს. სიჩქარე " + Integer.toString(radars.get(i).speed) + " კმ/სთ")
								.icon(BitmapDescriptorFactory.fromResource(R.drawable.radar)));
					}
					i++;
				}
			}
			is.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		buttonStartStop = (ImageButton) findViewById(R.id.imageButton1);
		buttonSettingsPage = (ImageButton) findViewById(R.id.buttonSettings);

		textLarge = (TextView) findViewById(R.id.textLARGESPEED);
		textDistance = (TextView) findViewById(R.id.textDistance);
		lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		buttonSettingsPage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent settingsIntent = new Intent(v.getContext(), SettingsActivity.class);
				v.getContext().startActivity(settingsIntent);
			}
		});

		Criteria criteria = new Criteria();

		provider = lm.getBestProvider(criteria, false);
		provider = LocationManager.GPS_PROVIDER;
		loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			showGPSDisabledAlertToUser();
		}
		buttonStartStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS, 0);
				distanceToStartBeeping = settings.getInt("distanceFromRadar", 100);
				// Toast.makeText(getApplicationContext(),
				// Integer.toString(distanceToStartBeeping),
				// Toast.LENGTH_LONG).show();

				if (!isOn) {// ჩართვა

					if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
						showGPSDisabledAlertToUser();
					} else {
						// lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,
						// 0, 1, loclist);
						startService(new Intent(MainActivity.this, BackgroundService.class));
						buttonStartStop.setImageResource(R.drawable.off);
						isOn = true;
					}

				} else if (isOn) {// გამორთვა
					buttonStartStop.setImageResource(R.drawable.on);
					isOn = false;
					if (BackgroundService.instance != null)
						BackgroundService.instance.stopRequestingGPS();
				}
			}
		});

	}

	private void showGPSDisabledAlertToUser() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Dialog));
		alertDialogBuilder.setMessage("თქვენს მოწყობილობას გათიშული აქვს GPS").setCancelable(false)
				.setPositiveButton("GPS-ის ჩართვა", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(callGPSSettingIntent);
					}
				});
		alertDialogBuilder.setNegativeButton("გაუქმება", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}

	// public void showFirstStartMessage() {
	// AlertDialog.Builder alertInternet = new AlertDialog.Builder(this);
	// alertInternet.setMessage(
	// "რუკის Offline რეჟიმში გამოსაყენებლად გთხოვთ დაუკავშირდეთ ინტერნეტს WiFi-ს ან მობილური ინტერნეტის დახმარებით")
	// .setCancelable(true);
	// AlertDialog alrt = alertInternet.create();
	// alrt.show();
	//
	// }

	public void noGooglePlayServices() {
		AlertDialog.Builder alertInternet = new AlertDialog.Builder(this);
		alertInternet.setMessage(
				"თქვენ არ გაქვთ დაყენებული Google Play Services გთხოვთ დააწვეთ ღილაკს  "
						+ "'Get Google Play Services', იმისათვის, რომ პროგრამამ სრულფასოვნად იმუშავოს").setCancelable(true);
		AlertDialog alrt = alertInternet.create();
		alrt.show();

	}

	@Override
	protected void onPause() {
		super.onPause();
		isAppOnMainScreen = false;

		if (BackgroundService.instance != null)
			BackgroundService.instance.stopRequestingGPS();

		wl.release();
	}

	@Override
	protected void onResume() {
		super.onResume();
		isAppOnMainScreen = true;
		if (BackgroundService.instance != null) {
			// BackgroundService.instance.startRequestingGPS();
			buttonStartStop.setImageResource(R.drawable.off);
			if (BackgroundService.tempLoc != null)
				locUpdate();
		}

		SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS, 0);
		if (settings.getBoolean("keepScreenAlive", true)) {
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		} else {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		wl.acquire();
	}

	@Override
	public void onBackPressed() {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Dialog));
		alertDialogBuilder.setMessage("გსურთ პროგრამიდან გამოსვლა?").setCancelable(false)
				.setPositiveButton("კი", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						if (BackgroundService.instance != null) {
							BackgroundService.instance.stopRequestingGPS();
						}
						System.exit(0);

					}
				});
		alertDialogBuilder.setNegativeButton("არა", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();

	}

	@Override
	protected void onDestroy() {
		isAppOnMainScreen = false;
		super.onDestroy();
	}

}