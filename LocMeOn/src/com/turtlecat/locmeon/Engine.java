package com.turtlecat.locmeon;

public class Engine {
	final static double earthRadius = 6371.16;
	
	public static String getDistance(double userLat, double lat2, double userLon, double lon2){
        double dLon = Math.toRadians(lon2-userLat);
        double dLat = Math.toRadians(lat2-userLon);

        double a = (Math.sin(dLat / 2) * Math.sin(dLat / 2)) +
                (Math.cos(Math.toRadians(userLat))) *
                        (Math.cos(Math.toRadians(lat2))) *
                        (Math.sin(dLon / 2)) *
                        (Math.sin(dLon / 2));
        double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return Integer.toString((int)Math.round(angle*earthRadius*1000));
    }
	
	public static int getClosest(){
		
		return 0;
	}
	public static void updateDB(){
		//ბაზის ინტერნეტიდან გადმოწერა
	}
	
	


}
