package com.turtlecat.locmeon;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends Activity {
	public static final String PREFS = "PREFS";
	public CheckBox keepScreenAlive,signalCheckBox;
	public static boolean screenAliveIsChecked=false;
	public static boolean signalWithoutSpeed=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.settings);

		
		Button buttonSaveSettings = (Button) findViewById(R.id.buttonSave);
		keepScreenAlive = (CheckBox) findViewById(R.id.checkBox1);
		signalCheckBox = (CheckBox) findViewById(R.id.checkBox2);
		final EditText inputDistance = (EditText) findViewById(R.id.editText1);
		
		final SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS, 0);
		final SharedPreferences.Editor editor = settings.edit();
		if(settings.getBoolean("keepScreenAlive", true)){
			screenAliveIsChecked=true;
			keepScreenAlive.setChecked(true);
		}
		if(settings.getBoolean("signalWithoutSpeed",false)){
			signalWithoutSpeed=true;
			signalCheckBox.setChecked(true);
		}
		
		
		inputDistance.setText(Integer.toString(settings.getInt("distanceFromRadar", 100)));
		//მონაცემების შენახვა
		buttonSaveSettings.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(inputDistance.getText().length()>0){
					int distanceFromRadar = Integer.parseInt(inputDistance.getText().toString());
					editor.putInt("distanceFromRadar", distanceFromRadar);
					String toastText = "მონაცემები შენახულია " + distanceFromRadar + " მეტრი";
					Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT).show();
				}
				
				editor.putBoolean("keepScreenAlive", keepScreenAlive.isChecked());
				editor.putBoolean("signalWithoutSpeed", signalCheckBox.isChecked());
				
				editor.commit();
			///	String toastText2 = Boolean.toString(keepScreenAlive.isChecked());
			}
			
		});
		
		
		

	}
}
